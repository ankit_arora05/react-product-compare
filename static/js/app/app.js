import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';
import compareApp from './components/products/reducers';
import { Router, Route, browserHistory, IndexRouter } from 'react-router';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';
import ProductComponent from './components/products/ProductComponent';
const store = createStore(
        combineReducers({
            global: compareApp,
            routing: routerReducer
        }), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

const history = syncHistoryWithStore(browserHistory, store);

render( 
    <Provider store={store}>
        <Router history={history}>
            <Route path="/" component="{ProductComponent}">
            </Route>
        </Router>
    </Provider>,
    document.getElementById('app')
);