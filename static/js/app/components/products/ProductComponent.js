import React, { PropTypes } from 'react';
class ProductComponent extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                <h1>testing my first react app</h1>
                {this.props.children}
            </div>
        )
    }
}

export default ProductComponent;