var express = require('express');
var path = require('path');
var app = express();

app.use('/static', express.static(path.join(__dirname, 'static')));

app.get('/', function(req,res){
    res.sendFile(path.join(__dirname + '/static/templates/index.html'));
});

app.listen(9000, function(){
    console.log('App is running on localhost:9000');
});